<?php

function get_taxonomy_meta($taxonomy_id, $key, $single = false)
{
    return get_metadata('taxonomy', $taxonomy_id, $key, $single);
}

function update_taxonomy_meta($taxonomy_id, $meta_key, $meta_value, $prev_value = '')
{
    return update_metadata('taxonomy', $taxonomy_id, $meta_key, $meta_value, $prev_value);
}

function add_taxonomy_meta($taxonomy_id, $meta_key, $meta_value, $unique = false)
{
    return add_metadata('taxonomy', $taxonomy_id, $meta_key, $meta_value, $unique);
}

function delete_taxonomy_meta($taxonomy_id, $meta_key, $meta_value = '')
{
    return delete_metadata('taxonomy', $taxonomy_id, $meta_key, $meta_value);
}

function ct_select($atts, $arr_values, $current_value = '', $select_one = true)
{
    $html_atts = '';

    switch (gettype($atts)) {
        case 'string':
            $html_atts = array('name="' . $atts . '"');
            break;
        case 'array':
            $html_atts = array();
            
            foreach ($atts as $key => $value) {
                $html_atts[] = $key . '="' . $value . '"';
            }

            break;
    }

    $output = '<select ' . implode(' ', $html_atts) . '>';

    $current_value = (array) $current_value;

    if ($select_one) {
        $output .= '<option value="">' . __('Select one', 'ct') . '</option>';
    }

    foreach ($arr_values as $value => $label) {
        $output .= '<option value="' . $value . '"' . (in_array($value, $current_value) ? ' selected="selected"' : '') . '>' . $label . '</option>';
    }

    $output .= '</select>';

    return $output;
}

function ct_checkbox($name, $arr_values, $current_values = array(), $default_values = array())
{
    $output = '';

    if (empty($current_values)) {
        $current_values = $default_values;
    }

    foreach ($arr_values as $value => $title) {
        $output .= '<input type="checkbox" name="' . $name . '[]" id="' . $name . '-' . sanitize_title($value) . '" value="' . $value . '" ' . (in_array($value, $current_values) ? 'checked="checked"' : '') . '> <label for="' . $name . '-' . sanitize_title($value) . '">' . __($title, 'ct') . '</label><br/>';
    }

    return $output;
}

function ct_is_qtranslate_enabled()
{
    global $q_config;
    
    return isset($q_config) && function_exists('qtransep_init');
}
