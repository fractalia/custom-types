<?php
if (basename($_SERVER['PHP_SELF']) != 'admin.php')
    die();
?>
<div class="wrap">
    <h2>
        <?php echo $this->title; ?>
        <?php echo isset($this->addNew) ? $this->addNew : ''; ?>
    </h2>

    <?php echo $this->alerts(); ?>

    <?php echo $this->content; ?>
</div>