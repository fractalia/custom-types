<?php
if (basename($_SERVER['PHP_SELF']) != 'admin.php') {
    die();
}

$this->title = __('Metaboxes', 'ct');
$this->addNew = '<a class="add-new-h2" href="' . admin_url('/admin.php?page=ct-metabox&action=add') . '">' . __('Add new', 'ct') . '</a>';
?>

<table cellspacing="0" class="wp-list-table widefat fixed">
    <thead>
        <tr>
            <th class="manage-column" scope="col"><?php _e('Metaboxes', 'ct'); ?></th>
            <th class="manage-column" scope="col"><?php _e('Post types', 'ct'); ?></th>
            <th class="manage-column" scope="col"><?php _e('Description', 'ct'); ?></th>
        </tr>
    </thead>

    <tfoot>
        <tr>
            <th class="manage-column" scope="col"><?php _e('Metaboxes', 'ct'); ?></th>
            <th class="manage-column" scope="col"><?php _e('Post types', 'ct'); ?></th>
            <th class="manage-column" scope="col"><?php _e('Description', 'ct'); ?></th>
        </tr>
    </tfoot>
    <tbody>
        <?php if (empty($this->metaboxes)): ?>
            <tr><td colspan="3"><?php _e('There are no custom post types', 'ct'); ?></td></tr>
        <?php else: ?>
            <?php foreach ($this->metaboxes as $key => $metabox): ?>
                <tr class="<?php echo $metabox['status']; ?>">
                    <td class="plugin-title"><strong><?php _e($metabox['title']); ?></strong><a title="" href="?page=ct-metabox&action=edit&id=<?php echo $key; ?>"><?php _e('Edit', 'ct'); ?></a> | <a title="" class="delete" href="?page=ct-metabox&action=delete&id=<?php echo $key; ?>"><?php _e('Delete', 'ct'); ?></a></td>
                    <td><?php echo implode(', ', $metabox['post_types']); ?></td>
                    <td class="desc">
                        <?php echo apply_filters('the_content', $metabox['description']); ?>
                    </td>
                </tr>
            <?php endforeach; ?>                
        <?php endif; ?>
    </tbody>
</table>