<?php
wp_enqueue_script('custom-types', $this->plugin_url . '/scripts.js');

if (!empty($this->metabox['description'])):
    echo apply_filters('the_excerpt', $this->metabox['description']);
endif;
?>

<table class="form-table">
    <tbody>
        <?php foreach ($this->metabox['fields'] as $field): ?>
            <tr>
                <th>
                    <label for=""><?php echo apply_filters('the_title', $field['label']); ?></label>
                </th>
                <td>
                    <?php
                    $field['value'] = get_post_meta($this->post_id, $field['name'], true);
                    $field['attributes']['name'] = 'ct[' . $field['name'] . ']';

                    echo $this->field($field);

                    if (isset($field['description'])):
                        echo apply_filters('the_content', $field['description']);
                    endif;
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>   