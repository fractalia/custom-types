<?php
$this->title = __('New metabox', 'ct');

wp_enqueue_style('chosen', $this->plugin_url . '/includes/chosen/chosen.min.css');
wp_enqueue_script('chosen', $this->plugin_url . '/includes/chosen/chosen.jquery.min.js');
?>

<form id="form-metabox" action="" method="post">
    <table class="form-table">
        <tbody>
            <tr>
                <th><?php _e('Name', 'ct'); ?></th>
                <td><input autocomplete="off" type="text" name="name" required/></td>
            </tr>
            <tr>
                <th><?php _e('Title', 'ct'); ?></th>
                <td><input autocomplete="off" type="text" name="title" <?php if (ct_is_qtranslate_enabled()): ?>data-multilanguage<?php endif; ?> required/></td>
            </tr>
            <tr>
                <th><?php _e('Description', 'ct'); ?></th>
                <td><textarea cols="20" rows="5" name="description" <?php if (ct_is_qtranslate_enabled()): ?>data-multilanguage<?php endif; ?>></textarea></td>
            </tr>
            <tr>
                <th><?php _e('Post types', 'ct'); ?></th>
                <td>
                    <?php
                    echo ct_select(array('multiple' => 'multiple', 'required' => 'required', 'name' => 'post_types[]'), get_post_types(array('public' => true), 'names'), array(), false);
                    ?>
                </td>
            </tr>
        </tbody>        
    </table>

    <div class="tablenav top">
        <div class="alignleft actions bulkactions">
            <a data-rel="add-field" class="button"><?php _e('Add new', 'ct'); ?></a>
        </div>
        <br class="clear">
    </div>

    <table id="metabox-list" cellspacing="0" class="wp-list-table widefat fixed">
        <thead>
            <tr>
                <th class="manage-column column-name" scope="col"><?php _e('Name', 'ct'); ?></th>
                <th class="manage-column column-description" scope="col"><?php _e('Label', 'ct'); ?></th>
                <th class="manage-column column-description" scope="col"><?php _e('Description', 'ct'); ?></th>
                <th class="manage-column column-posts num" scope="col"><?php _e('Type', 'ct'); ?></th>
                <th class="manage-column column-multilanguage" scope="col"><?php _e('Properties', 'ct'); ?></th>
                <th class="manage-column column-posts num" scope="col">&nbsp;</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th class="manage-column column-name" scope="col"><?php _e('Name', 'ct'); ?></th>
                <th class="manage-column column-description" scope="col"><?php _e('Label', 'ct'); ?></th>
                <th class="manage-column column-description" scope="col"><?php _e('Description', 'ct'); ?></th>
                <th class="manage-column column-posts num" scope="col"><?php _e('Type', 'ct'); ?></th>
                <th class="manage-column column-multilanguage" scope="col"><?php _e('Properties', 'ct'); ?></th>
                <th class="manage-column column-posts num" scope="col">&nbsp;</th>
            </tr>
        </tfoot>

        <tbody>
            <tr class="field-empty"><td colspan="<?php echo (5 + (int) ct_is_qtranslate_enabled()); ?>"><?php _e('Currently there are not fields for this metabox', 'ct'); ?></td></tr>
        </tbody>
    </table>
    <p class="submit"><input type="submit" class="button-primary" tabindex="21" value="<?php _e('Save changes', 'ct') ?>" /></p>
</form>
<?php ob_start(); ?>
<tr id="field-__index__">
    <td class="name column-name">
        <input autocomplete="off" type="text" name="fields[__index__][name]" required value="%name%"/>
    </td>
    <td class="description column-description">
        <input autocomplete="off" <?php if (ct_is_qtranslate_enabled()): ?>data-multilanguage<?php endif; ?> type="text" name="fields[__index__][label]" required value="%label%"/>
    </td>
    <td class="description column-description">
        <textarea <?php if (ct_is_qtranslate_enabled()): ?>data-multilanguage<?php endif; ?> name="fields[__index__][description]">%description%</textarea>
    </td>
    <td class="posts column-posts num">
        <?php echo ct_select(array('name' => 'fields[__index__][type]', 'required' => 'required'), $this->field_types, '', false); ?>
    </td>
    <td class="check-column">
        <label><input type="checkbox" name="fields[__index__][required]" /> <?php _e('Required', 'ct'); ?></label><br/>
        <?php if (ct_is_qtranslate_enabled()): ?>
            <label><input type="checkbox" name="fields[__index__][multilanguage]" /> <?php _e('Multilanguage', 'ct'); ?></label>
        <?php endif; ?>
    </td>
    <td class="posts column-posts num">
        <a data-rel="remove-field" href="#"><?php _e('Delete', 'ct'); ?></a>
    </td>
</tr>
<?php
$template = preg_replace("/\r|\n/", "", ob_get_contents());
ob_end_clean();
?>
<script>
    jQuery(document).ready(function($) {
        $('body').on('click', '[data-rel="add-field"]', function() {
            var template = '<?php echo $template; ?>';
            var index = $('table#metabox-list > tbody > tr:not(.field-empty)').length;

            if (index === 0) {
                $('table#metabox-list > tbody > tr.field-empty').hide();
            }

            $('table#metabox-list > tbody').append(template.replace(/__index__/gi, index).replace(/%[a-z]+%/gi, ''));

            <?php if (ct_is_qtranslate_enabled()): ?>
            qtransepGenerateMultilanguageElements(qtransepLanguages, qtransepFlags, '#wpbody-content');
            <?php endif; ?>

            return false;
        });

        $('body').on('click', '[data-rel="remove-field"]', function(evt) {
            $(evt.currentTarget).parents('tr').remove();
            return false;
        });

        $('select').chosen();
    });
</script>