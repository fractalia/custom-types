<form method="post" action="">            
    <h3><?php _e('Basic options', 'ct'); ?></h3>

    <?php wp_nonce_field('ct_add_custom_post_type'); ?>

    <p>The name of the post-type cannot be changed. The name may show up in your URLs, e.g. ?movie=star-wars. This will also make a new theme file available, starting with prefix named "single-", e.g. single-movie.php.</p>

    <table class="form-table">
        <tr valign="top">
            <th scope="row"><?php _e('Name', 'ct') ?></th>
            <td>
                <input autocomplete="off" type="text" name="name" tabindex="1" value="<?php echo isset($custom_post_type['name']) ? esc_attr($custom_post_type['name']) : ''; ?>" />  <?php printf(__('(e.g. %s, preferably in English)', 'ct'), '<i>movie</i>'); ?>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row"><?php _e('Label', 'ct') ?></th><td><input autocomplete="off" type="text" name="label" tabindex="2" value="<?php echo isset($custom_post_type['label']) ? esc_attr($custom_post_type['label']) : ''; ?>" multilanguage /> (e.g. Movies)</td>
        </tr>
        <tr valign="top">
            <th scope="row"><?php _e('Singular label', 'ct') ?></th><td><input autocomplete="off" type="text" name="singular_label" tabindex="3" value="<?php echo isset($custom_post_type['singular_label']) ? esc_attr($custom_post_type['singular_label']) : ''; ?>" multilanguage /> (e.g. Movie)</td>
        </tr>
        <tr valign="top">
            <th scope="row"><?php _e('Description', 'ct') ?></th>
            <td><textarea name="description" tabindex="4" rows="2" cols="35"><?php echo isset($custom_post_type['description']) ? esc_attr($custom_post_type['description']) : ''; ?></textarea></td>
        </tr>
        <tr valign="top"><th scope="row"><?php _e('Icon', 'ct') ?></th><td><?php echo ct_select(array('name' => 'menu_icon', 'style' => 'width:200px', 'path' => WP_PLUGIN_URL . '/' . $this->_plugin_basename . '/icons'), array(), $custom_post_type['menu_icon'], false); ?> <span style="display:block; float:right;width:16px; height:16px; margin-top:5px; background-position:left top; background-repeat:no-repeat; background-image:url(<?php echo WP_PLUGIN_URL . '/' . $this->_plugin_basename . '/icons/' . (isset($custom_post_type['menu_icon']) ? $custom_post_type['menu_icon'] : 'default.png'); ?>)"></span></td></tr>
        <tr valign="top"><th scope="row"><?php _e('Status', 'ct') ?></th><td><?php echo ct_select('status', array('active' => 'Active', 'inactive' => 'Inactive'), $custom_post_type['status'], false); ?></td></tr>
    </table>

    <h3><?php _e('Advanced options', 'ct'); ?></h3>

    <table class="form-table">
        <tr valign="top"><th scope="row"><?php _e('Public', 'ct') ?></th><td><?php echo ct_select('public', array('true' => 'Yes', 'false' => 'No'), $custom_post_type['public'], false); ?></td></tr>
        <tr valign="top"><th scope="row"><?php _e('Show UI', 'ct') ?></th><td><?php echo ct_select('show_ui', array('true' => 'Yes', 'false' => 'No'), $custom_post_type['show_ui'], false); ?></td></tr>
        <tr valign="top"><th scope="row"><?php _e('Capability type', 'ct') ?></th><td><input type="text" name="capability_type" tabindex="6" value="post" value="<?php echo esc_attr($custom_post_type['capability']); ?>" /></td></tr>
        <tr valign="top"><th scope="row"><?php _e('Hierarchical', 'ct') ?></th><td><?php echo ct_select('hierarchical', array('false' => 'No', 'true' => 'Yes'), $custom_post_type['hierarchical'], false); ?></td></tr>
        <tr valign="top"><th scope="row"><?php _e('Rewrite', 'ct') ?></th><td><?php echo ct_select('rewrite', array('true' => 'Yes', 'false' => 'No'), $custom_post_type['rewrite'], false); ?></td></tr>
        <tr valign="top"><th scope="row"><?php _e('Custom rewrite slug', 'ct') ?></th><td><input type="text" name="rewrite_slug" tabindex="9" value="<?php echo isset($custom_post_type['rewrite_slug']) ? esc_attr($custom_post_type['rewrite_slug']) : ''; ?>" /></td></tr>
        <tr valign="top"><th scope="row"><?php _e('Query var', 'ct') ?></th><td><?php echo ct_select('query_var', array('true' => 'Yes', 'false' => 'No'), $custom_post_type['query_var'], false); ?></td></tr>
        <tr valign="top"><th scope="row"><?php _e('Menu position', 'ct') ?></th>
            <td>
                <?php
                echo ct_select('menu_position', array(
                    '5' => 'Bellow Posts',
                    '10' => 'Below Media',
                    '20' => 'Below Pages',
                    '60' => 'Below first separator',
                    '100' => 'Below second separator'
                        ), $custom_post_type['menu_position'], false);
                ?>
            </td>
        </tr>
        <tr valign="top"><th scope="row"><?php _e('Supports', 'ct') ?></th><td>
                <div style="height:110px; overflow-y:scroll; border:1px solid #DFDFDF; padding-left:5px">
                    <?php
                    echo ct_checkbox('supports', array(
                        'title' => 'title',
                        'editor' => 'editor',
                        'excerpt' => 'excerpt',
                        'trackbacks' => 'trackbacks',
                        'custom-fields' => 'custom-fields',
                        'comments' => 'comments',
                        'revisions' => 'revisions',
                        'thumbnail' => 'thumbnail',
                        'author' => 'author',
                        'page-attributes' => 'page-attributes',
                        'show_in_nav_menus' => 'show_in_nav_menus'
                            ), $custom_post_type['supports'], array(
                        'title' => 'title',
                        'editor' => 'editor',
                        'thumbnail' => 'thumbnail',
                        'author' => 'author',
                    ));
                    ?>
                </div>
            </td></tr>
        <tr valign="top"><th scope="row"><?php _e('Built-in Taxonomies', 'ct') ?></th><td>
                <div style="height:110px; overflow-y:scroll; border:1px solid #DFDFDF; padding-left:5px">
                    <?php
                    $taxonomies = array();
                    foreach (get_taxonomies(array('public' => true), 'names') as $taxonomy) {
                        if ($taxonomy != 'nav_menu') {
                            $taxonomies[$taxonomy] = $taxonomy;
                        }
                    }
                    echo ct_checkbox('taxonomies', $taxonomies, $custom_post_type['taxonomies']);
                    ?>
                </div>
            </td></tr>
        <tr valign="top"><th scope="row"><?php _e('Custom posts per page', 'ct') ?></th><td><input type="text" name="posts_per_page" value="<?php echo isset($custom_post_type['posts_per_page']) ? esc_attr($custom_post_type['posts_per_page']) : ''; ?>" /></td></tr>
        <?php $orderby = array('none' => __('None', 'ct'), 'ID' => __('ID', 'ct'), 'author' => __('Autor', 'ct'), 'title' => __('Title', 'ct'), 'date' => __('Date', 'ct'), 'modified' => __('Modified', 'ct'), 'parent' => __('Parent', 'ct'), 'rand' => __('Random', 'ct'), 'comment_count' => __('Comment', 'ct'), 'menu_order' => __('Order', 'ct')); ?>
        <tr valign="top"><th scope="row"><?php _e('Order by', 'ct') ?></th><td><?php echo ct_select('orderby', $orderby, $custom_post_type['orderby'] || 'menu_order', false); ?> <?php echo ct_select('order', array('ASC' => __('Ascending', 'ct'), 'DESC' => __('Descending', 'ct')), $custom_post_type['order'] || 'ASC', false); ?></td></tr>
    </table>

    <br/>

    <strong><?php _e('Labels', 'ct'); ?></strong>

    <table class="form-table">
        <?php foreach ($this->_labels['post_type'] as $key => $label): ?>
            <tr valign="top"><th scope="row"><?php echo $key; ?></th><td><input autocomplete="off" type="text" name="labels[<?php echo $key; ?>]" value="<?php echo $custom_post_type['labels'][$key] || ''; ?>" multilanguage /> (e.g. "<?php printf($label[1], __($custom_post_type[$label[0]])); ?>")</td></tr>
        <?php endforeach; ?>
    </table>

    <p class="submit"><input type="submit" class="button-primary" tabindex="21" value="<?php _e('Save changes', 'ct') ?>" /></p>
</form>