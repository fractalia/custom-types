<?php
if (basename($_SERVER['PHP_SELF']) != 'admin.php')
    die();

$post_types = get_option('cp_post_types', array());
?>
<div class="wrap">
    <div class="icon32" id="icon-options-general"><br></div>
    <h2>
        <?php _e('Custom Post Types', 'ct'); ?>
        <a class="add-new-h2" href="<?php echo admin_url('admin.php?page=ct-post-type&action=edit'); ?>"><?php _e('Add new', 'ct'); ?></a>
    </h2>

    <table cellspacing="0" class="wp-list-table widefat fixed">
        <thead>
            <tr>
                <th class="manage-column" scope="col"><?php _e('Post type', 'ct'); ?></th>
                <th class="manage-column" scope="col"><?php _e('Description', 'ct'); ?></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th class="manage-column" scope="col"><?php _e('Post type', 'ct'); ?></th>
                <th class="manage-column" scope="col"><?php _e('Description', 'ct'); ?></th>
            </tr>
        </tfoot>
        <tbody>
            <?php if (empty($post_types)): ?>
                <tr><td colspan="2"><?php _e('There are no custom post types', 'ct'); ?></td></tr>
            <?php else: ?>
                <?php foreach ($post_types as $post_type): ?>
                    <tr class="<?php echo $post_type['status']; ?>">
                        <td class="plugin-title"><strong><?php _e($post_type['label']); ?> (<?php echo $post_type['name']; ?>)</strong><a title="" href="?page=ct-edit-post&id=<?php echo $post_type['id']; ?>"><?php _e('Edit', 'ct'); ?></a> | <a title="" class="delete" href="?page=ct&element=post&id=<?php echo $post_type['id']; ?>"><?php _e('Delete', 'ct'); ?></a></td>
                        <td class="desc">
                            <p><?php echo $post_type['description']; ?></p>
                        </td>
                    </tr>
                <?php endforeach; ?>                
            <?php endif; ?>
        </tbody>
    </table>
</div>