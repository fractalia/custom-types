<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of custom_types
 *
 * @author José Carlos Chávez <jose.carlos.chavez@sysco.no>
 */
class CT_Bootstrap
{

    protected $dirpath;

    /**
     *
     * @var CT_VIew
     */
    private $view_response = null;

    public function __construct()
    {
        $this->dirpath = __DIR__;

        $this->setup_database();

        load_plugin_textdomain('ct', false, $this->dirpath . '/languages');

        add_action('init', array($this, 'init'));
        add_action('admin_menu', array($this, 'create_admin_menu'));
        add_action('admin_init', array($this, 'register_metaboxes'));

        add_action('save_post', array($this, 'save_post'));
    }

    public static function run()
    {
        new self();
    }

    private function setup_database()
    {
        global $wpdb;
        $wpdb->taxonomymeta = $wpdb->prefix . 'taxonomymeta';
    }

    public function register_metaboxes()
    {
        $metaboxes = CT_Service_Metabox::get_metaboxes();

        foreach ($metaboxes as $metabox) {
            foreach ($metabox['post_types'] as $post_type) {
                add_meta_box('metabox-' . $metabox['name'], apply_filters('the_title', $metabox['title']), array($this, 'render_meta_box'), $post_type, 'normal', 'high');
            }
        }
    }

    public function render_meta_box($post, $metabox)
    {
        $this->dispatch_request('metabox', 'box', array(
            'post_id' => $post->ID,
            'metabox_id' => substr($metabox['id'], 8)
        ));

        $this->view_response->set_layout(null);

        $this->render_view();
    }

    public function create_admin_menu()
    {
        add_menu_page('Custom Types', 'Custom Types', 'administrator', 'ct', array($this, 'render_view'), plugins_url(__FILE__) . '/images/icon.png');
        add_submenu_page('ct', 'Post Types', __('Post types', 'ct'), 'administrator', 'ct-post_type', array($this, 'render_view'));
        add_submenu_page('ct', 'Taxonomies', __('Taxonomies', 'ct'), 'administrator', 'ct-taxonomy', array($this, 'render_view'));
        add_submenu_page('ct', 'Custom fields', __('Custom fields', 'ct'), 'administrator', 'ct-metabox', array($this, 'render_view'));
    }

    public function save_post($post_id, $post)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;

        foreach ($_POST['ct'] as $name => $value) {
            update_post_meta($post_id, $name, $value);
        }
    }

    public function init()
    {
        if (!is_admin() || !isset($_GET['page']) || (is_admin() && !(substr($_GET['page'], 0, 3) === 'ct-' || $_GET['page'] === 'ct'))) {
            return;
        }

        $controller = substr($_GET['page'], 3);

        if (empty($controller)) {
            $controller = 'default';
        }

        $action = isset($_GET['action']) ? $_GET['action'] : 'index';

        $this->dispatch_request($controller, $action);
    }

    public function dispatch_request($controller_name, $action, $request_params = array())
    {
        $controller_classname = 'CT_Controller_' . ct_underscore_to_camel_case($controller_name, true);

        $controller = new $controller_classname();

        $controller->set_request(new CT_Request($request_params));

        $this->view_response = $controller->dispatch($action);
    }

    public function render_view()
    {
        if (null !== $this->view_response) {
            echo $this->view_response->render();
            return;
        }

        wp_die('Could not resolve this controller/action.');
    }

    public function message()
    {
        
    }

}
