<?php

/**
 * Description of metabox
 *
 * @author jcchavezs
 */
class CT_Controller_Metabox extends CT_Controller
{

    public function index_action()
    {
        $metaboxes = $this->get_service('metabox')->get_metaboxes();

        return array(
            'metaboxes' => $metaboxes
        );
    }

    public function add_action()
    {
        if ($this->request->is_post()) {
            $data = $this->request->get_post();

            $metaboxes = get_option('ct_metaboxes', array());

            $name = sanitize_title($data['name']);
            
            $metaboxes[$name] = $data;

            update_option('ct_metaboxes', $metaboxes);

            wp_redirect('/wp-admin/admin.php?page=ct-metabox&action=index');
        }

        return array(
            'field_types' => $this->get_service('metabox')->get_field_types()
        );
    }

    public function edit_action()
    {
        $metaboxes = get_option('ct_metaboxes', array());

        if ($this->request->is_post()) {
            $data = $this->request->get_post();

            foreach($data['fields'] as $key => $value){
                $value['multilanguage'] = (isset($value['multilanguage']) && $value['multilanguage'] === 'on' ? true : false);
                
                $data['fields'][$key] = $value;
            }
            
            $metaboxes[$data['name']] = $data;

            update_option('ct_metaboxes', $metaboxes);

            wp_redirect('admin.php?page=ct-metabox&action=index');
        }

        $metabox_id = $this->request->get_param('id');

        return array(
            'metabox' => $metaboxes[$metabox_id],
            'field_types' => $this->get_service('metabox')->get_field_types()
        );
    }

    public function delete_action()
    {
        $metaboxes = get_option('ct_metaboxes', array());

        if (!is_null($id = $this->request->get_param('id'))) {
            unset($metaboxes[$id]);

            update_option('ct_metaboxes', $metaboxes);

            wp_redirect('/wp-admin/admin.php?page=ct-metabox&action=index');
        }
    }

    public function box_action()
    {
        $post_id = $this->request->get_param('post_id');
        $metabox_id = $this->request->get_param('metabox_id');

        $metaboxes = $this->get_service('metabox')->get_metaboxes();

        $metabox = $metaboxes[$metabox_id];
        
        return array(
            'post_id' => $post_id,
            'metabox' => $metabox
        );
    }

}
