<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of field
 *
 * @author José Carlos Chávez <jose.carlos.chavez@sysco.no>
 */
class CT_Field
{

    public function __construct(array $field)
    {
        foreach ($field as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function get_html_attributes()
    {
        return array();
    }

    public function setup_html_attributes()
    {
        $this->attributes['id'] = uniqid();
                
        if (ct_is_qtranslate_enabled() && $this->multilanguage) {
            $this->attributes['data-multilanguage'] = true;
        }
        
        if($this->required){
            $this->attributes['required'] = true;
        }

        $this->attributes += $this->get_html_attributes();
    }

    protected function render_html_attributes($exclude = array())
    {
        $this->setup_html_attributes();

        $html_attributes = array();

        foreach ($this->attributes as $key => $value) {
            if(in_array($key, $exclude)){
                continue;
            }
            
            switch (gettype($value)) {
                case 'boolean':
                    if ($value === true) {
                        $html_attributes[] = "{$key}";
                    }
                    break;
                default:
                    $html_attributes[] = "{$key}='{$value}'";
                    break;
            }
        }

        return implode(' ', $html_attributes);
    }

    public function render()
    {
        $attributes = $this->render_html_attributes();

        return "<input {$attributes} value='{$this->value}' class='regular-text'/>";
    }

    public function __toString()
    {
        return $this->render();
    }

}
