<?php

class CT_Service_Metabox
{

    public static function get_field_types()
    {
        return array(
            'text' => __('Text', 'ct'),
            'date' => __('Date', 'ct'),
            'number' => __('Number', 'ct'),
            'email' => __('E-mail', 'ct'),
            'url' => __('Url', 'ct'),
            'phone' => __('Phone', 'ct'),
            'skype' => __('Skype', 'ct'),
            'textarea' => __('Textarea', 'ct'),
            'wysiwyg' => __('WYSIWYG', 'ct'),
            'file' => __('File', 'ct'),
            'select' => __('Select', 'ct'),
            'multiselect' => __('Multiselect', 'ct'),
            'checkbox' => __('Checkbox', 'ct'),
            'multicheckbox' => __('Multicheckbox', 'ct'),
            'radio' => __('Radio', 'ct')
        );
    }

    public static function get_metaboxes()
    {
        return get_option('ct_metaboxes', array());
    }

}
