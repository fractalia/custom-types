<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of view
 *
 * @author jcchavezs
 */
class CT_View
{

    protected $scripts = '';
    protected $styles = '';
    protected $layout = 'default';
    protected $plugin_url = '';

    public function __construct()
    {
        $this->plugin_url = plugins_url('', dirname(__FILE__));
    }

    public function set_variables($variables)
    {
        foreach ($variables as $key => $value) {
            $this->{$key} = $value;
        }

        return $this;
    }

    public function set_variable($key, $value)
    {
        $this->{$key} = $value;
        return $this;
    }

    public function set_layout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    public function set_script($script)
    {
        $this->script = $script;
        return $this;
    }

    public function alerts()
    {
        
    }

    public function add_script($key)
    {
        $script_args = array_pad(func_get_args(), 5, null);

        $script_args[4] = true;

        $this->scripts[$key] = $script_args;

        return $this;
    }

    public function add_style($key)
    {
        $style_args = array_pad(func_get_args(), 5, null);

        $style_args[4] = true;

        $this->styles[$key] = $style_args;

        return $this;
    }

    public function enqueue_styles()
    {
        foreach ($this->styles as $style) {
            call_user_func_array('wp_enqueue_style', $style);
        }
    }

    public function enqueue_scripts()
    {
        foreach ($this->scripts as $script) {
            call_user_func_array('wp_enqueue_script', $script);
        }
    }

    public function render()
    {
        ob_start();
        $view_filename = $this->script . '.php';
        require_once dirname(__DIR__) . '/views/scripts/' . $view_filename;
        $this->content = ob_get_contents();
        ob_end_clean();

        if (null === $this->layout) {
            echo $this->content;
        } else {
            add_action('admin_enqueue_styles', array($this, 'enqueue_styles'));
            add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts'));

            require_once dirname(__DIR__) . '/views/layouts/' . $this->layout . '.php';
        }
    }

    public function __call($name, $arguments)
    {
        $helper_classname = 'CT_View_Helper_' . ucfirst($name);
        $helper = new $helper_classname();
        return call_user_func_array(array($helper, $name), $arguments);
    }

}
