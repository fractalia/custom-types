<?php

/**
 * Description of CT_Field_Textarea
 *
 * @author jcchavezs
 */
class CT_Field_Textarea extends CT_Field
{

    public function get_html_attributes()
    {
        return array(
            'rows' => 4,
            'cols' => 50
        );
    }
    
    public function render()
    {
        $attributes = $this->render_html_attributes();

        return "<textarea {$attributes}>{$this->value}</textarea>";
    }

}
