<?php

/**
 * Description of CT_Field_Textarea
 *
 * @author jcchavezs
 */
class CT_Field_Wysiwyg extends CT_Field_Textarea
{    

    public function render()
    {
        $textarea_html_attributes = $this->render_html_attributes(array('id', 'data-multilanguage'));

        ob_start();
        ?>
        <div id="<?php echo $this->attributes['id']; ?>" data-ct="wysiwyg" data-qtransep-type="wysiwyg" <?php echo ($this->attributes['data-multilanguage']) ? 'data-multilanguage' : ''; ?>>
            <textarea <?php echo $textarea_html_attributes ?>><?php echo $this->value; ?></textarea>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }

}
