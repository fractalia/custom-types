<?php

/**
 * Description of CT_Field_Text
 *
 * @author jcchavezs
 */
class CT_Field_Text extends CT_Field
{

    public function get_html_attributes()
    {
        return array(
            'type' => 'text'
        );
    }

}
