<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of controller
 *
 * @author jcchavezs
 */
class CT_Controller
{

    protected $request;
    protected $services = array();

    public function set_request(CT_Request $request)
    {
        $this->request = $request;
        return $this;
    }

    public function dispatch($action_name)
    {
        $controller_name_pieces = explode('_', get_class($this));
        
        $controller_name = array_pop($controller_name_pieces);

        $this->view = new CT_View();

        $this->view->set_script(strtolower($controller_name) . '/' . $action_name);

        $variables = $this->{$action_name . '_action'}();

        return $this->view->set_variables($variables);
    }

    protected function get_service($name)
    {
        if (!array_key_exists($name, $this->services)) {
            $this->service = new CT_Service_Metabox();
        }

        return $this->service;
    }

}
