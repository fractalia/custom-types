<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of field
 *
 * @author jcchavezs
 */
class CT_View_Helper_Field
{

    public function field($field)
    {
        switch ($field['type']) {
            case 'textarea':
                return new CT_Field_Textarea($field);
                break;
            case 'wysiwyg':
                return new CT_Field_Wysiwyg($field);
                break;
            case 'date':
                return new CT_Field_Date($field);
                break;
            default:
                return new CT_Field_Text($field);
                break;
        }
    }

}
