<?php

class CT_Request
{

    public function __construct(array $params = array())
    {
        $this->params = array_merge($_REQUEST, $params);
    }

    public function is_post()
    {
        return ($_SERVER['REQUEST_METHOD'] === 'POST');
    }

    public function get_post()
    {
        return $_POST;
    }
    
    public function get_params()
    {
        return $this->params;
    }

    public function get_param($name, $default = false)
    {
        return array_key_exists($name, $this->params) ? $this->params[$name] : $default;
    }

}
