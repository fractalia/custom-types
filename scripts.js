jQuery(document).ready(function($) {
    $('[data-ct=wysiwyg]:not([data-multilanguage]) > textarea').each(function(i, e) {
        var textareaId = $(e).parent().attr('id') + '-textarea';

        $(e)
                .attr('id', textareaId)
                .addClass('mceEditor');

        if (typeof (tinyMCE) === 'object' && typeof (tinyMCE.execCommand) === 'function') {
            tinyMCE.execCommand('mceAddControl', false, textareaId);            
        }
    });
});
