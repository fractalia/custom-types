<?php

/*
  Plugin Name: Custom types
  Description: Admin panel for creating custom post types and custom taxonomies in WordPress
  Author: Fractalia - Applications lab
  Version: 0.1
  Author URI: http://fractalia.pe
  Tags: fractalia, wordpress, custom, plugin, post type, taxonomy, custom post type, custom taxonomy
 *
 * License: GNU General Public License, v2 (or newer)
 * License URI:  http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * Thanks to http://randyjensenonline.com/thoughts/wordpress-custom-post-type-fugue-icons/ for the icon pack.
 */

function ct_register_activation_hook()
{
    global $wpdb;
    $wpdb->query("CREATE TABLE `{$wpdb->taxonomymeta}` (
                        `meta_id` bigint(20) unsigned NOT NULL auto_increment,
                        `taxonomy_id` bigint(20) unsigned NOT NULL default '0',
                        `meta_key` varchar(255) default NULL,
                        `meta_value` longtext,
                        PRIMARY KEY  (`meta_id`),
                        KEY `taxonomy_id` (`taxonomy_id`),
                        KEY `meta_key` (`meta_key`)
       )");
}

register_activation_hook(__FILE__, 'ct_register_activation_hook');

function ct_underscore_to_camel_case($string, $first_char_caps = false)
{
    if ($first_char_caps == true) {
        $string[0] = strtoupper($string[0]);
    }

    $func = create_function('$c', 'return strtoupper($c[1]);');

    return preg_replace_callback('/_([a-z])/', $func, $string);
}

function ct_camel_case_to_dash($string)
{
    $string[0] = strtolower($string[0]);
    $func = create_function('$c', 'return "-" . strtolower($c[1]);');
    return preg_replace_callback('/([A-Z])/', $func, $string);
}

function ct_autoloader($classname)
{
    if (substr($classname, 0, 3) === 'CT_') {
        $filepath = str_replace(array('_-', '_'), '/', ct_camel_case_to_dash(substr($classname, 3))) . '.php';
        require_once __DIR__ . '/classes/' . strtolower($filepath);
    }
}

spl_autoload_register('ct_autoloader');

function ct_load_textdomain()
{
    load_plugin_textdomain('ct', false, dirname(plugin_basename(__FILE__)) . '/languages');
}

add_action('plugins_loaded', 'ct_load_textdomain');

require_once 'functions.php';

CT_Bootstrap::run();
